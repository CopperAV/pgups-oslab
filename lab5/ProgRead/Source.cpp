// ��� ������ ������������ ������ �5, ����� 1.
#include <windows.h>
#include <tchar.h>
#include <iostream>
#include <math.h>
#include <bitset>
#include <random>
#include <time.h>

bool dialog(char);
char dialog_choice();
void dialog_menu();
void dialog_vol_info();
void dialog_file_atribute();
void dialog_change_file_atribute();
void dialog_change_external_drive_label();
void dialog_write_to_file();
void dialog_create_file();
void dialog_test_time();
void dialog_close_programm();

bool volumeInstalled(char);
bool volumeInstalled(char, bool);

DWORD printVoulmeInfo(_TCHAR*);

void printVolumeFlags(DWORD);
void printFileAtribute(DWORD);

void printDWORDdata(DWORD, char**);
void printDWORDdata(DWORD, char**, short);

void code_generator(HANDLE, int);
void primeMath(int &n);

using namespace std;

LARGE_INTEGER PAMtime = {0};

int _tmain(int argc, _TCHAR* argv[])
{
	using namespace std;
	bool exitflag;
	
	do
	{
		cout << "This is Lab work #5 - Exploring the file system and asynchronous file operations" << endl;
		system("TITLE Main menu");
		dialog_menu();
		char choice = dialog_choice();
		exitflag = dialog(choice);
		cout << endl;
		system("pause");
		system("CLS");
	} while (!exitflag);

	return 0;
}

// �������: ������ � ������������� (1 ��������)
bool dialog(char choice)
{
	switch (choice)
	{
		case '1': dialog_vol_info(); break;
		case '2': dialog_file_atribute(); break;
		case '3': dialog_change_file_atribute();  break;
		case '4': dialog_change_external_drive_label(); break;
		case '5': dialog_write_to_file();  break;
		case '6': dialog_create_file(); break;
		case '7': dialog_test_time(); break;
		case '8': dialog_close_programm(); return true;
		default:  cout << "What you want? How you jump to this code?" << endl;
	}
	return false;
}

// �������: ������ �� ������ �������� �� ����
char dialog_choice() {
	char s = '\0';
	
	cout << endl << "Write number from 1 to 8: ";
	cin >> s;
	while (cin.fail() || !(s >= '1' && s <= '8'))
	{
		cout << endl << "Write number from 1 to 8!" << endl;
		cout << "Your answer: ";
		cin >> s;
	}

	cout << endl;

	return s;
}

// ���������: ����� ����, ����������� ��� ������
void dialog_menu() {
	cout << endl << "Menu:" << endl
		<< "1) Get volume information" << endl
		<< "2) Get file atributes." << endl
		<< "3) Change file atributes." << endl
		<< "4) Change external volume name." << endl
		<< "5) Write to file" << endl
		<< "6) Create new text file." << endl
		<< "7) Write to text file generated code (around 1000K) and execute programm from lab 1" << endl
		<< "8) Exit form programm." << endl;
}

// ���������: ��������� ��������� ����������� ����
void dialog_vol_info() {
	system("TITLE Get volume information");
	char s;

	cout << endl << "Write volume name (A-Z) or \'0\' to print all: ";
	cin >> s;

	bool ans = false;

	if (s != '0') {
		ans = volumeInstalled(s);
		if (!ans) {
			cout << "Volume not installed." << endl;
			return;
		}

		_TCHAR a[4] = L"#:\\";
		a[0] = _TCHAR(s);

		DWORD flags = printVoulmeInfo(a);
		printVolumeFlags(flags);
	} else {
		for (char ch = 'A'; ch <= 'Z'; ch++) {
			ans = volumeInstalled(ch);

			if (!ans) continue;

			_TCHAR a[4] = L"#:\\";
			a[0] = _TCHAR(ch);

			DWORD flags = printVoulmeInfo(a);
		}
	}
}

// ���������: ��������� ��������� �����
void dialog_file_atribute() {
	system("TITLE Get file atributes");
	char s[255] = { 0 };
	_TCHAR b[255] = { 0 };

	cout << endl << "Write path to file with format: ";
	cin >> s;

	bool ans = false;

	ans = volumeInstalled(s[0]);
	if (!ans) {
		cout << "Volume not installed." << endl;
		return;
	}

	for (int i = 0; i < 255 && s[i] != '\0'; i++)
		b[i] = _TCHAR(s[i]);

	DWORD flags = GetFileAttributes(b);

	printFileAtribute(flags);
}

// ���������: ����� ��������� �����
void dialog_change_file_atribute() {
	system("TITLE Change file atributes");

	char s[255] = { 0 };
	_TCHAR b[255] = { 0 };

	cout << endl << "Write path to file with format: ";
	cin >> s;

	bool ans = false;

	ans = volumeInstalled(s[0]);
	if (!ans) {
		cout << "Volume not installed." << endl;
		return;
	}

	for (int i = 0; i < 255 && s[i] != '\0'; i++)
		b[i] = _TCHAR(s[i]);

	cout << endl;
	DWORD flags = GetFileAttributes(b);

	char** arr = new char*[8];
	arr[0] = new char[45]{ "FILE_ATTRIBUTE_READONLY                     " }; //0x01
	arr[1] = new char[45]{ "FILE_ATTRIBUTE_HIDDEN                       " }; //0x02
	arr[2] = new char[45]{ "FILE_ATTRIBUTE_SYSTEM                       " }; //0x04
	arr[3] = new char[45]{ "?                                           " }; //0x08
	arr[4] = new char[45]{ "FILE_ATTRIBUTE_DIRECTORY                    " }; //0x10
	arr[5] = new char[45]{ "FILE_ATTRIBUTE_ARCHIVE                      " }; //0x20
	arr[6] = new char[45]{ "FILE_ATTRIBUTE_DEVICE                       " }; //0x40
	arr[7] = new char[45]{ "FILE_ATTRIBUTE_NORMAL                       " }; //0x80

	printDWORDdata(flags, arr, (short)0x01);

	char fileformat;

	do {
		cout << "Select atribute (READONLY - 0, HIDDEN - 1, ARCHIVE - 2, NORMAL - 3): " << endl;
		cout << "Your answer: ";
		cin >> fileformat;
	} while (cin.fail() || !(fileformat >= '0' && fileformat <= '4'));

	//����� ���������

	DWORD inflag = flags & (FILE_ATTRIBUTE_READONLY | FILE_ATTRIBUTE_HIDDEN | FILE_ATTRIBUTE_ARCHIVE | FILE_ATTRIBUTE_NORMAL);
	if (fileformat == '0') {
		if (inflag == FILE_ATTRIBUTE_NORMAL)
			inflag = FILE_ATTRIBUTE_READONLY;
		else if (inflag & FILE_ATTRIBUTE_READONLY)
			inflag = inflag & (FILE_ATTRIBUTE_HIDDEN | FILE_ATTRIBUTE_ARCHIVE);
		else inflag = inflag | FILE_ATTRIBUTE_READONLY;
	} else if (fileformat == '1') {
		if (inflag == FILE_ATTRIBUTE_NORMAL)
			inflag = FILE_ATTRIBUTE_HIDDEN;
		else if (inflag & FILE_ATTRIBUTE_HIDDEN)
			inflag = inflag & (FILE_ATTRIBUTE_READONLY | FILE_ATTRIBUTE_ARCHIVE);
		else inflag = inflag | FILE_ATTRIBUTE_HIDDEN;
	} else if (fileformat == '2') {
		if (inflag == FILE_ATTRIBUTE_NORMAL)
			inflag = FILE_ATTRIBUTE_ARCHIVE;
		else if (inflag & FILE_ATTRIBUTE_ARCHIVE)
			inflag = inflag & (FILE_ATTRIBUTE_READONLY | FILE_ATTRIBUTE_HIDDEN);
		else inflag = inflag | FILE_ATTRIBUTE_ARCHIVE;
	} else inflag = FILE_ATTRIBUTE_NORMAL;

	SetFileAttributes(b, inflag);

	flags = GetFileAttributes(b);
	printDWORDdata(flags, arr, (short)0x01);
}

// ���������: ����� ����� ���� �������� ��������
void dialog_change_external_drive_label() {
	system("TITLE Change external volume name");

	char root = { 0 };

	cout << endl << "Write external volume symbol (A-Z): ";
	cin >> root;

	bool ans = false;

	ans = volumeInstalled(root);
	if (!ans) {
		cout << "Volume not installed." << endl;
		return;
	}

	_TCHAR a[4] = L"#:\\";
	a[0] = _TCHAR(root);

	UINT typeofvolume = GetDriveType(a);
	if (typeofvolume != DRIVE_REMOVABLE) {
		cout << "It's not a removable drive, operation interrupted." << endl;
		return;
	}

	char s[11] = { 0 };
	_TCHAR b[11] = { 0 };

	cout << endl << "Write new label name (up to 10 characters): ";
	cin >> s;
	s[11] = '\0';

	for (int i = 0; i < 11 && s[i] != '\0'; i++)
		b[i] = _TCHAR(s[i]);

	bool res = SetVolumeLabel(a, b);

	if (!res) {
		cout << "Operation not complete" << endl;
	}
}

void dialog_write_to_file() {
	system("TITLE Write to file");

	char s[255] = { 0 };
	_TCHAR b[255] = { 0 };

	cout << endl << "Write path to file with format: ";
	cin >> s;

	bool ans = false;

	ans = volumeInstalled(s[0]);
	if (!ans) {
		cout << "Volume not installed." << endl;
		return;
	}

	for (int i = 0; i < 255 && s[i] != '\0'; i++)
		b[i] = _TCHAR(s[i]);

	HANDLE file = CreateFile(b,                     //Open file on path
					(GENERIC_READ | GENERIC_WRITE), //With Read/write rights
					FILE_SHARE_READ,				//Other programm can read this file
					NULL,							//Default security
					OPEN_EXISTING,					//Open, not create
					NULL,							//Atribute will be ignored
					NULL);							//Atribute will be ignored
	
	if (file == INVALID_HANDLE_VALUE) {
		cout << endl  << "File not open. " << endl;
		DWORD err = GetLastError();
		if (err & ERROR_FILE_NOT_FOUND) cout << "Reason: File not found."  << endl;
		if (err & ERROR_FILE_READ_ONLY) cout << "Reason: File with readolny rights." << endl;
		return;
	}

	char message[20] = "My new text message";
	LPCVOID buf = &message;
	DWORD len = 20;
	DWORD added = { 0 };

	WriteFile(file,buf,len,&added,NULL);

	cout << added << endl;
}

void dialog_create_file() {
	system("TITLE Create new file");

	char s[255] = { 0 };
	_TCHAR b[255] = { 0 };

	cout << endl << "Write path to new file with format: ";
	cin >> s;

	bool ans = false;

	ans = volumeInstalled(s[0]);
	if (!ans) {
		cout << "Volume not installed." << endl;
		return;
	}

	for (int i = 0; i < 255 && s[i] != '\0'; i++)
		b[i] = _TCHAR(s[i]);

	HANDLE file = CreateFile(b,         //Open file on path
		(GENERIC_READ | GENERIC_WRITE), //With Read/write rights
		FILE_SHARE_READ,				//Other programm can read this file
		NULL,							//Default security
		CREATE_NEW,						//Open, not create
		FILE_ATTRIBUTE_NORMAL,			//Atribute will be ignored
		NULL);							//Atribute will be ignored

	if (file == INVALID_HANDLE_VALUE) {
		cout << endl << "File not open. " << endl;
		DWORD err = GetLastError();
		if (err & ERROR_FILE_EXISTS) cout << "Reason: File exists." << endl;
		return;
	}

	cout << "File on path: " << b << " - created" << endl;
	CloseHandle(file);
}

struct ExecuteTimePGM {
	long long sec;
	long long msec;
	long long microsec;
};

ExecuteTimePGM findTime(LARGE_INTEGER StartingTime, LARGE_INTEGER EndingTime) {
	ExecuteTimePGM ans;
	LARGE_INTEGER ElapsedMicroseconds;
	LARGE_INTEGER Frequency;

	QueryPerformanceFrequency(&Frequency);

	ElapsedMicroseconds.QuadPart = EndingTime.QuadPart - StartingTime.QuadPart;

	ans.sec = ElapsedMicroseconds.QuadPart / Frequency.QuadPart;
	ans.msec = ElapsedMicroseconds.QuadPart % Frequency.QuadPart;
	ans.microsec = ans.msec % 10000;
	ans.msec /= 10000;

	return ans;
}

void dialog_test_time() {
	system("TITLE test time");

	char s[255] = { 0 };
	_TCHAR b[255] = { 0 };

	cout << endl << "Write path to new file with format: ";
	cin >> s;

	bool ans = false;

	ans = volumeInstalled(s[0]);
	if (!ans) {
		cout << "Volume not installed." << endl;
		return;
	}

	for (int i = 0; i < 255 && s[i] != '\0'; i++)
		b[i] = _TCHAR(s[i]);

	char choiceChar;
	int choiceInt;
	cout << "Choice buffer size:" << endl
		<< "[0] - 512; [1] - 1024; [2] - 2048; [3] - 4096" << endl;
	cin >> choiceChar;

	switch (choiceChar) {
	case '0': choiceInt = 512; break;
	case '2': choiceInt = 2048; break;
	case '3': choiceInt = 4096; break;
	default: choiceInt = 1024;
	}

	HANDLE file = CreateFile(b,         //Open file on path
		(GENERIC_READ | GENERIC_WRITE), //With Read/write rights
		FILE_SHARE_READ,				//Other programm can read this file
		NULL,							//Default security
		CREATE_NEW,						//Create new file
		FILE_ATTRIBUTE_NORMAL,			//Normal file
		NULL);							//Atribute will be ignored

	if (file == INVALID_HANDLE_VALUE) {
		cout << endl << "File not open. " << endl;
		DWORD err = GetLastError();
		if (err & ERROR_FILE_EXISTS) cout << "Reason: File exists." << endl;
		return;
	}

	system("TITLE test time phase 1: synch test Write to file");

	LARGE_INTEGER StartingTime, EndingTime;

	QueryPerformanceCounter(&StartingTime);
	code_generator(file, choiceInt);
	CloseHandle(file);
	QueryPerformanceCounter(&EndingTime);

	ExecuteTimePGM CSF = findTime(StartingTime, EndingTime);

	DeleteFile(b);

	system("TITLE test time phase 1: synch test Prime math");

	int n = 1000000;
	QueryPerformanceCounter(&StartingTime);
	primeMath(n);
	QueryPerformanceCounter(&EndingTime);

	ExecuteTimePGM PSM = findTime(StartingTime, EndingTime);

	system("TITLE test time phase 2: asynch test Prime math + Write to file");

	file = CreateFile(b,         //Open file on path
		(GENERIC_READ | GENERIC_WRITE), //With Read/write rights
		FILE_SHARE_READ,				//Other programm can read this file
		NULL,							//Default security
		CREATE_NEW,						//Create new file
		FILE_ATTRIBUTE_NORMAL,			//Normal file
		NULL);							//Atribute will be ignored

	HANDLE h = CreateThread(
		NULL,
		NULL,
		(LPTHREAD_START_ROUTINE)primeMath,
		&n,
		CREATE_SUSPENDED,
		NULL);

	QueryPerformanceCounter(&StartingTime);
	ResumeThread(h);
	code_generator(file, choiceInt);
	CloseHandle(file);
	WaitForSingleObject(h, NO_ERROR);
	QueryPerformanceCounter(&EndingTime);

	ExecuteTimePGM TAMF = findTime(StartingTime, EndingTime);

	DeleteFile(b);

	cout << " Create 100K file time: " << CSF.sec << " s; " << CSF.msec << " ms; " << CSF.microsec << " micro;" << endl;
	cout << " Execute N=" << n << " prime time: " << PSM.sec << " s; " << PSM.msec << " ms; " << PSM.microsec << " micro;" << endl;
	cout << " Full time: " << CSF.sec + PSM.sec << " s; " << CSF.msec + PSM.msec << " ms; " << CSF.microsec + PSM.microsec << " micro;" << endl;
	cout << " Full time for 2 thread: " << TAMF.sec << " s; " << TAMF.msec << " ms; " << TAMF.microsec << " micro;" << endl;
}

void primeMath(int &n) {
	int N = n;
	int *a = new int[N + 1];
	for (int i = 0; i < N + 1; i++)
		a[i] = i;
	for (int p = 2; p < N + 1; p++)
	{
		if (a[p] != 0)
		{
			cout << "last prime: " << a[p] << '\r';
			for (long long j = (long long)p * (long long)p; j < N + 1; j += p)
				a[j] = 0;
		}
	}
	delete a;
	cout << endl;
}

void code_generator(HANDLE file, int bufferSize) {
	LPVOID buf;
	DWORD len = bufferSize+2;
	DWORD added = { 0 };
	_TCHAR* translate = new _TCHAR[bufferSize+2];
	char* kilomessage = new char[bufferSize];

	int size = bufferSize / 512;
	int iterSize = int(double(2 / size) * 1024);
	int lastIter = iterSize * 100;
	for (int j = 0; j < lastIter; j++) {
		srand(j);
		for (int i = 0; i < bufferSize; i++) {
			int answer = rand() % 16;
			if (answer >= 0 && answer <= 9)
				kilomessage[i] = answer + '0';
			else
				kilomessage[i] = (answer - 10) + 'A';
			translate[i] = _TCHAR(kilomessage[i]);
		}
		translate[bufferSize] = _TCHAR('\n');
		translate[bufferSize+1] = _TCHAR('\r');
		buf = &translate;
		WriteFile(file, buf, len, &added, NULL);
		if (j % iterSize == 0)
			cout << "Add " << j/iterSize << "/" << 100 << " MB"  << '\r';
	}
	cout << "Add 100/100 MB" << endl;
}

// ���������: ����� �� ���������
void dialog_close_programm() {
	system("TITLE Exit from program");
	cout << "Bye!" << endl;
}

// ������ ��� �������, ��� ������ � �������
bool volumeInstalled(char volume) {
	return volumeInstalled(volume, true);
}

//��������, ���������� �� ���������� ���.
bool volumeInstalled(char volume, bool printinfo) {
	if (volume >= 'a' && volume <= 'z') volume += abs('a' - 'A');
	else if (volume < 'A' || volume > 'Z') {
		if (printinfo) cout << "Unknown volume symbol: \'"<< volume << "\'" << endl;
		return false;
	}

	DWORD volumeWord = GetLogicalDrives();
	DWORD volumeKnown = 0;

	int bit = (volume - 'A');
	volumeKnown = (DWORD)pow(2, bit);

	if (volumeWord & volumeKnown) return true;
	else return false;
}

//����� ���������� � �����
DWORD printVoulmeInfo(_TCHAR* volume) {
	_TCHAR volName[80] = { 0 };
	_TCHAR FSName[80] = { 0 };

	LPWSTR VolumeS = volName, FileSystemS = FSName;
	const DWORD VolumeL = 80, FileSystemL = 80;

	DWORD SerialNum = 0, VolFlags = 0, VolPathSize = 0;
	
	LPDWORD VolumeSerial = &SerialNum,
		VolumePathL = &VolPathSize,
		FileSystemFlags = &VolFlags;

	bool ans = GetVolumeInformation(volume,
		VolumeS, VolumeL, VolumeSerial, VolumePathL,
		FileSystemFlags, FileSystemS, FileSystemL);

	cout << "- - - - - - - - - - - - - - - - - - - - - - - - - - - - -" << endl
		<< " System information:" << endl 
		<< "    Volume: " << (char)volume[0] << endl
		<< "    Label: ";
	
	for (int i = 0; i < 80 && (char)volName[i] != '\0'; i++)
		cout << (char)volName[i];

	cout << endl << "    File System: ";

	for (int i = 0; i < 80 && (char)FSName[i] != '\0'; i++)
		cout << (char)FSName[i];

	cout << endl << "    Serial Number: "
		<< uppercase << hex << ((SerialNum & 0xFFFF0000) >> 16)
		<< "-" << (SerialNum & 0x0000FFFF) << endl
		<< nouppercase << "    Path max size: "
		<< dec << VolPathSize << endl;
	return VolFlags;
}

// ������ ���������� �� ������ ����.
void printVolumeFlags(DWORD VolFlags) {
	char** arr = new char*[32];
	arr[ 0] = new char[45]{ "FILE_CASE_SENSITIVE_SEARCH:                 " }; //0x01
	arr[ 1] = new char[45]{ "FILE_CASE_PRESERVED_NAMES:                  " }; //0x02
	arr[ 2] = new char[45]{ "FILE_UNICODE_ON_DISK:                       " }; //0x04
	arr[ 3] = new char[45]{ "FILE_PERSISTENT_ACLS:                       " }; //0x08
	arr[ 4] = new char[45]{ "FILE_FILE_COMPRESSION:                      " }; //0x10
	arr[ 5] = new char[45]{ "FILE_VOLUME_QUOTAS:                         " }; //0x20
	arr[ 6] = new char[45]{ "FILE_SUPPORTS_SPARSE_FILES:                 " }; //0x40
	arr[ 7] = new char[45]{ "FILE_SUPPORTS_REPARSE_POINTS:               " }; //0x80

	arr[ 8] = new char[45]{ "?                                           " }; //0x01
	arr[ 9] = new char[45]{ "?                                           " }; //0x02
	arr[10] = new char[45]{ "?                                           " }; //0x04
	arr[11] = new char[45]{ "?                                           " }; //0x08
	arr[12] = new char[45]{ "?                                           " }; //0x10
	arr[13] = new char[45]{ "?                                           " }; //0x20
	arr[14] = new char[45]{ "?                                           " }; //0x40
	arr[15] = new char[45]{ "FILE_VOLUME_IS_COMPRESSED:                  " }; //0x80

	arr[16] = new char[45]{ "FILE_SUPPORTS_OBJECT_IDS:                   " }; //0x01
	arr[17] = new char[45]{ "FILE_SUPPORTS_ENCRYPTION:                   " }; //0x02
	arr[18] = new char[45]{ "FILE_NAMED_STREAMS:                         " }; //0x04
	arr[19] = new char[45]{ "FILE_READ_ONLY_VOLUME:                      " }; //0x08
	arr[20] = new char[45]{ "FILE_SEQUENTIAL_WRITE_ONCE:                 " }; //0x10
	arr[21] = new char[45]{ "FILE_SUPPORTS_TRANSACTIONS:                 " }; //0x20
	arr[22] = new char[45]{ "FILE_SUPPORTS_HARD_LINKS:                   " }; //0x40
	arr[23] = new char[45]{ "FILE_SUPPORTS_EXTENDED_ATTRIBUTES:          " }; //0x80

	arr[24] = new char[45]{ "FILE_SUPPORTS_OPEN_BY_FILE_ID:              " }; //0x01
	arr[25] = new char[45]{ "FILE_SUPPORTS_USN_JOURNAL:                  " }; //0x02
	arr[26] = new char[45]{ "?                                           " }; //0x04
	arr[27] = new char[45]{ "?                                           " }; //0x08
	arr[28] = new char[45]{ "?                                           " }; //0x10
	arr[29] = new char[45]{ "FILE_DAX_VOLUME:                            " }; //0x20
	arr[30] = new char[45]{ "?                                           " }; //0x40
	arr[31] = new char[45]{ "?                                           " }; //0x80

	printDWORDdata(VolFlags, arr);
}

// ������ ���������� �� ������ �����
void printFileAtribute(DWORD FileFlags) {
	char** arr = new char*[32];
	arr[ 0] = new char[45]{ "FILE_ATTRIBUTE_READONLY                     " }; //0x01
	arr[ 1] = new char[45]{ "FILE_ATTRIBUTE_HIDDEN                       " }; //0x02
	arr[ 2] = new char[45]{ "FILE_ATTRIBUTE_SYSTEM                       " }; //0x04
	arr[ 3] = new char[45]{ "?                                           " }; //0x08
	arr[ 4] = new char[45]{ "FILE_ATTRIBUTE_DIRECTORY                    " }; //0x10
	arr[ 5] = new char[45]{ "FILE_ATTRIBUTE_ARCHIVE                      " }; //0x20
	arr[ 6] = new char[45]{ "FILE_ATTRIBUTE_DEVICE                       " }; //0x40
	arr[ 7] = new char[45]{ "FILE_ATTRIBUTE_NORMAL                       " }; //0x80

	arr[ 8] = new char[45]{ "FILE_ATTRIBUTE_TEMPORARY                    " }; //0x01
	arr[ 9] = new char[45]{ "FILE_ATTRIBUTE_SPARSE_FILE                  " }; //0x02
	arr[10] = new char[45]{ "FILE_ATTRIBUTE_REPARSE_POINT                " }; //0x04
	arr[11] = new char[45]{ "FILE_ATTRIBUTE_COMPRESSED                   " }; //0x08
	arr[12] = new char[45]{ "FILE_ATTRIBUTE_OFFLINE                      " }; //0x10
	arr[13] = new char[45]{ "FILE_ATTRIBUTE_NOT_CONTENT_INDEXED          " }; //0x20
	arr[14] = new char[45]{ "FILE_ATTRIBUTE_ENCRYPTED                    " }; //0x40
	arr[15] = new char[45]{ "FILE_ATTRIBUTE_INTEGRITY_STREAM             " }; //0x80

	arr[16] = new char[45]{ "FILE_ATTRIBUTE_VIRTUAL                      " }; //0x01
	arr[17] = new char[45]{ "FILE_ATTRIBUTE_NO_SCRUB_DATA                " }; //0x02
	arr[18] = new char[45]{ "FILE_ATTRIBUTE_RECALL_ON_OPEN               " }; //0x04
	arr[19] = new char[45]{ "?                                           " }; //0x08
	arr[20] = new char[45]{ "?                                           " }; //0x10
	arr[21] = new char[45]{ "?                                           " }; //0x20
	arr[22] = new char[45]{ "FILE_ATTRIBUTE_RECALL_ON_DATA_ACCESS        " }; //0x40
	arr[23] = new char[45]{ "?                                           " }; //0x80

	arr[24] = new char[45]{ "?                                           " }; //0x01
	arr[25] = new char[45]{ "?                                           " }; //0x02
	arr[26] = new char[45]{ "?                                           " }; //0x04
	arr[27] = new char[45]{ "?                                           " }; //0x08
	arr[28] = new char[45]{ "?                                           " }; //0x10
	arr[29] = new char[45]{ "?                                           " }; //0x20
	arr[30] = new char[45]{ "?                                           " }; //0x40
	arr[31] = new char[45]{ "?                                           " }; //0x80

	printDWORDdata(FileFlags, arr);
}

// ������ ��� ������ ������ ���� ������
void printDWORDdata(DWORD flags, char** textdata){
	printDWORDdata(flags, textdata, (short)0xFF);
}

// ��������� ��� ������ ������
void printDWORDdata(DWORD flags, char** textdata, short printbyte){
	bitset<4> highhalf, lowhalf;
	byte bword;

	if (printbyte != 0) {
		cout << "Flags: " << endl
			<< "- - - - - - - - - - - - - - - - - - - - - - - - - - - - -\n"
			<< "Byte:   Flag name:                                status:\n";
	}

	if (printbyte & 0x01) {
		bword = (byte)(flags & 0x000000FF);
		lowhalf = bitset<4>(bword);
		highhalf = bitset<4>(bword >> 4);

		cout << "Byte 1: |XXXX XXXX|XXXX XXXX|XXXX XXXX|" << highhalf << " " << lowhalf << "|\n"
			<< "0x01  - " << textdata[ 0] << ((bword & 0x01) ? " ON " : " OFF") << endl
			<< "0x02  - " << textdata[ 1] << ((bword & 0x02) ? " ON " : " OFF") << endl
			<< "0x04  - " << textdata[ 2] << ((bword & 0x04) ? " ON " : " OFF") << endl
			<< "0x08  - " << textdata[ 3] << ((bword & 0x08) ? " ON " : " OFF") << endl
			<< "0x10  - " << textdata[ 4] << ((bword & 0x10) ? " ON " : " OFF") << endl
			<< "0x20  - " << textdata[ 5] << ((bword & 0x20) ? " ON " : " OFF") << endl
			<< "0x40  - " << textdata[ 6] << ((bword & 0x40) ? " ON " : " OFF") << endl
			<< "0x80  - " << textdata[ 7] << ((bword & 0x80) ? " ON " : " OFF") << endl;
	}

	if (printbyte & 0x02) {
		bword = (byte)((flags & 0x0000FF00) >> 8);
		lowhalf = bitset<4>(bword);
		highhalf = bitset<4>(bword >> 4);

		cout << "Byte 2: |XXXX XXXX|XXXX XXXX|" << highhalf << " " << lowhalf << "|XXXX XXXX|\n"
			<< "0x01  - " << textdata[ 8] << ((bword & 0x01) ? " ON " : " OFF") << endl
			<< "0x02  - " << textdata[ 9] << ((bword & 0x02) ? " ON " : " OFF") << endl
			<< "0x04  - " << textdata[10] << ((bword & 0x04) ? " ON " : " OFF") << endl
			<< "0x08  - " << textdata[11] << ((bword & 0x08) ? " ON " : " OFF") << endl
			<< "0x10  - " << textdata[12] << ((bword & 0x10) ? " ON " : " OFF") << endl
			<< "0x20  - " << textdata[13] << ((bword & 0x20) ? " ON " : " OFF") << endl
			<< "0x40  - " << textdata[14] << ((bword & 0x40) ? " ON " : " OFF") << endl
			<< "0x80  - " << textdata[15] << ((bword & 0x80) ? " ON " : " OFF") << endl;
	}

	if (printbyte & 0x04) {
		bword = (byte)((flags & 0x00FF0000) >> 16);
		lowhalf = bitset<4>(bword);
		highhalf = bitset<4>(bword >> 4);

		cout << "Byte 3: |XXXX XXXX|" << highhalf << " " << lowhalf << "|XXXX XXXX|XXXX XXXX|\n"
			<< "0x01  - " << textdata[16] << ((bword & 0x01) ? " ON " : " OFF") << endl
			<< "0x02  - " << textdata[17] << ((bword & 0x02) ? " ON " : " OFF") << endl
			<< "0x04  - " << textdata[18] << ((bword & 0x04) ? " ON " : " OFF") << endl
			<< "0x08  - " << textdata[19] << ((bword & 0x08) ? " ON " : " OFF") << endl
			<< "0x10  - " << textdata[20] << ((bword & 0x10) ? " ON " : " OFF") << endl
			<< "0x20  - " << textdata[21] << ((bword & 0x20) ? " ON " : " OFF") << endl
			<< "0x40  - " << textdata[22] << ((bword & 0x40) ? " ON " : " OFF") << endl
			<< "0x80  - " << textdata[23] << ((bword & 0x80) ? " ON " : " OFF") << endl;
	}

	if (printbyte & 0x08) {
		bword = (byte)((flags & 0xFF000000) >> 24);
		lowhalf = bitset<4>(bword);
		highhalf = bitset<4>(bword >> 4);

		cout << "Byte 4: |" << highhalf << " " << lowhalf << "|XXXX XXXX|XXXX XXXX|XXXX XXXX|\n"
			<< "0x01  - " << textdata[24] << ((bword & 0x01) ? " ON " : " OFF") << endl
			<< "0x02  - " << textdata[25] << ((bword & 0x02) ? " ON " : " OFF") << endl
			<< "0x04  - " << textdata[26] << ((bword & 0x04) ? " ON " : " OFF") << endl
			<< "0x08  - " << textdata[27] << ((bword & 0x08) ? " ON " : " OFF") << endl
			<< "0x10  - " << textdata[28] << ((bword & 0x10) ? " ON " : " OFF") << endl
			<< "0x20  - " << textdata[29] << ((bword & 0x20) ? " ON " : " OFF") << endl
			<< "0x40  - " << textdata[30] << ((bword & 0x40) ? " ON " : " OFF") << endl
			<< "0x80  - " << textdata[31] << ((bword & 0x80) ? " ON " : " OFF") << endl;
	}
}
