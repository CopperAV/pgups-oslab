//��������� � ��� ������������ ������ �2
#include <windows.h>
#include <tchar.h>
#include <thread>
#include <iostream>
#include <string>

void findprime();
void writePrime();
void writeData(int i);

using namespace std;

int n = 10000;
int k = 0;
int* A = new int[n + 1];

long printCounter = 0;

HANDLE hSemaphore, h, h2;

int _tmain(int argc, _TCHAR* argv[])
{
	setlocale(LC_ALL, "ru-RU"); //������� ����
	A[0] = 0;
	A[1] = 0;

	h = CreateThread(
		NULL,
		NULL,
		(LPTHREAD_START_ROUTINE)findprime,
		NULL,
		NULL,
		NULL);

	h2 = CreateThread(
		NULL,
		NULL,
		(LPTHREAD_START_ROUTINE)writePrime,
		NULL,
		NULL,
		NULL);

	hSemaphore = CreateSemaphore(
		NULL,	// ��� ��������
		1,		// ��������� ���������
		1,		// ������������ ���������
		NULL	// ��� �����
	);

	WaitForSingleObject(h, INFINITE);

	TerminateThread(h, NO_ERROR);

	system("pause");
}

void findprime() {
	while (true) {
		k = 0;
		//������� ������
		A[0] = 0;
		A[1] = 0;
		for (int i = 2; i < n + 1; i++)
			A[i] = i;

		for (int p = 2; p < n + 1 && k < 1000; p++)
			if (A[p] != 0){
				for (int j = p * p; j < n + 1; j += p)
					A[j] = 0;
				k++;
				if (k % 100 == 0 && k > 0)
					WaitForSingleObject(hSemaphore, INFINITE);
			}
	}
}

void writePrime(){
	int t = 0;
	int l = 0;
	while (true)
		if (t < k) {
			if (A[l] != 0) {
				t++;
				if (t % 100 == 0 && t > 0)
					writeData(l);
			}
			l++;
		}
		else if ((t == k && t == 1000) || t > k) {
			t = 0;
			l = 0;
		}
}

void writeData(int i){
	printCounter++;
	cout << "[" << printCounter << "]: " << i << endl;
	ReleaseSemaphore(hSemaphore, 1, NULL);
}