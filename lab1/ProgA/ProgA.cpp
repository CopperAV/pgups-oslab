//����� ������ ��������� � ������� ��������� ������� CreateProcess
//��������� � ��� ������������ ������ �1
#include <windows.h>		//��������� � ��������� �������
#include <tchar.h>		//������� _tmain � _TCHAR
#include <iostream>

using namespace std;

int _tmain(int argc, _TCHAR* argv[])
{
	setlocale(LC_ALL, "ru-RU"); //������� ����

	STARTUPINFO si = { 0 };			//������� ���������
	PROCESS_INFORMATION pi = { 0 };	//������� ���������

	cout << endl << "������� ��������." << endl;
	if (CreateProcess(
		(_TCHAR*)"ProgB.exe",	// ��� ������������ ������
		NULL,				// ��������� ������
		NULL,			// ��������� �� ��������� SECURITY_ATTRIBUTES
		NULL,			// ��������� �� ��������� SECURITY_ATTRIBUTES
		FALSE,		// ���� ������������ �������� ��������
		CREATE_NEW_CONSOLE,	// ����� �������� �������� ��������
		NULL,			// ��������� �� ���� �����
		NULL,			// ������� ���� ��� �������
		&si,			// ��������� ��� ��������� STARTUPINFO
		&pi			// ��������� ��� ��������� PROCESS_INFORMATION 
	) == TRUE)			// ���� ������� (��������� �) ������, ��:
	{
		Sleep(5000);	// ��������� 5 ������
		TerminateProcess(pi.hProcess, NO_ERROR);	// ������ �������
		cout << endl << "������� ��������." << endl;
	}
	else			//�����, �������� �� ����.
	{
		cout << "������� �� ������." << endl;
		return -1;	//����� �� ������ ��������� � ������ ���������.
	}

	system("pause");	//����� �������, ��� ������� ����������.
	return 0;		//��������� ��������� ��� ������.
}
