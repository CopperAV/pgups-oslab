//��������� � ��� ������������ ������ �1
#include <windows.h>
#include <tchar.h>
#include <thread>
#include <iostream>
#include <string>

int findprime();
int contextMenu();

using namespace std;

int n = 10000;
int localK = 0;
int& k = localK;
int* A = new int[n + 1];
int maxBeforeSleep = 0;
int maxBeforeResume = 0;
HANDLE Globh;

int _tmain(int argc, _TCHAR* argv[])
{
	setlocale(LC_ALL, "ru-RU"); //������� ����

	for (int i = 0; i < n + 1; i++)
		A[i] = i;
	A[1] = 0;

	HANDLE h = CreateThread(
		NULL,
		NULL, 
		(LPTHREAD_START_ROUTINE)findprime,
		NULL,
		CREATE_SUSPENDED,
		NULL);
	Globh = h;
	int command = 0;
	int j = 0, p = 0;
	int l = 0;
	string Sname;
	bool exitFlag = false;

	while (true)
	{
		if (p == 1000) break; //����� �� �����

		//������ � ����������� ������
		command = contextMenu();
		Sleep(50);

		switch (command)
		{
		case 1:
			cout << "������� ���������� ����� ��� ������� (> 0): ";
			
			cin >> Sname;
			

			cout << endl;
			
			try
			{
				// string -> integer
				maxBeforeSleep = std::stoi(Sname);
			}
			catch (exception e)
			{
				maxBeforeSleep = 1;
			}

			if (maxBeforeSleep < 1)
			{
				maxBeforeSleep = 1;
			}
			else if (maxBeforeSleep + k > 1000)
			{
				maxBeforeSleep = 1000 - k;
			}
			break;
		case 2:
			maxBeforeSleep = 1000 - k;
			break;
		case 3:
			exitFlag = true;
			break;
		// 0 - ��� ����� �� ���������
		case 0:
		default:
			cout << "����������� ��������, ������ 1 �����" << endl;
			maxBeforeSleep = 1;
		}

		maxBeforeResume = maxBeforeSleep;

		if (exitFlag) break;

		ResumeThread(h);

		for (;p < 1000 && maxBeforeResume > 0;)
		{
			if (p < k)
			{
				if (A[j] != 0)
				{
					p++;
					cout << p << ": " << A[j] << endl;
					maxBeforeResume--;
				}
				j++;
			}
		}

	}
	TerminateThread(h, NO_ERROR);

	system("pause");
}

int findprime() {
	cout << "������ ������ �" << endl;
		
	for (int p = 2; p < n + 1 && k < 1000; p++)
	{
		if (maxBeforeSleep <= 0)
			SuspendThread(Globh);

		if (A[p] != 0)
		{
			k++;
			for (int j = p * p; j < n + 1; j += p)
				A[j] = 0;

			maxBeforeSleep--;
		}
	}
	
	cout << "����� ������ �" << endl;
	return 0;
}

// ��� ������ ����������, ��� ������ � �������������� �������.
int contextMenu()
{
	cout << "�������� ��������: " << endl
		<< "[1] - ��������� N �����" << endl
		<< "[2] - ��������� ��� ���������� �����" << endl
		<< "[3] - ��������� ���������� �������" << endl;

	string Character;
	cin >> Character;

	int command;

	if (Character == "1")
		command = 1;
	else if (Character == "2")
		command = 2;
	else if (Character == "3")
		command = 3;
	else
		command = 0;

	return command;
}