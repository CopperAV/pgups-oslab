//��������� � ��� ������������ ������ �3
#include <windows.h>
#include <tchar.h>
#include <thread>
#include <iostream>
#include <string>

void maxSizeInfo(SIZE_T);
void testReAlloc();
void testFreeAndAlloc();
void testMyPoolSize();
void testMovePool();

HANDLE generalPool;
DWORD heapAllocFlag = HEAP_ZERO_MEMORY,
	  createHeapFlag = 0, 
	  heapReAllocFlag = HEAP_ZERO_MEMORY,
	  heapFreeFlag = 0;
SIZE_T megabyte=0x100000, kilobyte = 0x400, maxSizeFA = 0, maxSizeRA = 0, maxSizePA = 0;

using namespace std;

int _tmain(int argc, _TCHAR* argv[])
{
	generalPool = GetProcessHeap();

	cout << "ReAlloc test" << endl;
	testReAlloc();

	cout << endl;
	cout << "Free old and Alloc new test" << endl;
	testFreeAndAlloc();

	cout << endl;
	cout << "Alloc in pool new block test" << endl;
	testMyPoolSize();

	cout << endl;
	cout << "Move data from static pool to dynamic test" << endl;
	testMovePool();

	system("pause");
	return 0;
}

void maxSizeInfo(SIZE_T value) {
	cout << "Max size, that can be allocated: " << value << " byte" << endl;
	cout << "It's a " << value / 1024 << " KB" << endl;
	cout << "It's a " << (value / 1024) / 1024 << " MB" << endl;
}

void testReAlloc() {
	SIZE_T allocSize = megabyte;
	LPVOID myAllocPool = HeapAlloc(generalPool, heapAllocFlag, allocSize);
	LPVOID newReAllocPool = myAllocPool;

	while (newReAllocPool != NULL) {
		myAllocPool = newReAllocPool;
		allocSize += megabyte;
		newReAllocPool = HeapReAlloc(generalPool, heapReAllocFlag, myAllocPool, allocSize);
	}

	maxSizeRA = allocSize - megabyte;
	HeapFree(generalPool, heapFreeFlag, myAllocPool);

	maxSizeInfo(maxSizeRA);
}

void testFreeAndAlloc() {
	SIZE_T allocSize = megabyte;
	LPVOID myAllocPool = HeapAlloc(generalPool, heapAllocFlag, allocSize);

	while (myAllocPool != NULL) {
		HeapFree(generalPool, heapFreeFlag, myAllocPool);
		allocSize += megabyte;
		myAllocPool = HeapAlloc(generalPool, heapAllocFlag, allocSize);
	}

	maxSizeFA = allocSize - megabyte;
	HeapFree(generalPool, heapFreeFlag, myAllocPool);

	maxSizeInfo(maxSizeFA);
}

void testMyPoolSize() {
	HANDLE pool = HeapCreate(createHeapFlag, megabyte, 0);
	LPVOID* myPoolArray = new LPVOID[2048];
	int i = -1;

	do {
		i++;
		myPoolArray[i] = HeapAlloc(pool, heapAllocFlag, megabyte);
	} while (myPoolArray[i] != NULL);

	maxSizePA = megabyte * i;
	delete myPoolArray;
	HeapDestroy(pool);

	maxSizeInfo(maxSizePA);
}

void testMovePool() {
	HANDLE myPool = HeapCreate(createHeapFlag, megabyte, 0);
	LPVOID generalAlloc = HeapAlloc(generalPool, heapAllocFlag, kilobyte);
	LPVOID dynamicAlloc = HeapAlloc(myPool, heapAllocFlag, kilobyte);

	int *memoryBlockA = (int *)generalAlloc;
	int *memoryBlockB = (int *)dynamicAlloc;
	
	for (int i = 0; i < 10; i++)
		for (int j = 0; j < 10; j++)
			memoryBlockA[i * 10 + j] = i * 10 + j;

	for (int i = 0; i < 10; i++) {
		for (int j = 0; j < 10; j++)
			cout << memoryBlockA[i * 10 + j] << (memoryBlockA[i * 10 + j] > 9 ? "  " : "   ");
		cout << endl;
	}

	cout << endl;

	for (int i = 0; i < 10; i++)
		for (int j = 0; j < 10; j++)
			if (i == j) memoryBlockB[i] = memoryBlockA[i * 10 + j];

	for (int i = 0; i < 10; i++) 
		cout << memoryBlockB[i] << (memoryBlockB[i] > 9 ? "  " : "   ");
}