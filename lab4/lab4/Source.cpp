//��������� ��� ������������ ������ �4
#include <windows.h>
#include <tchar.h>
#include <thread>
#include <iostream>
#include <string>

int fixException(unsigned int code, struct _EXCEPTION_POINTERS *ep);

using namespace std;

const SIZE_T page_size = 4096;
const SIZE_T countOfPage = 16;
SIZE_T alloc_reserve_size = page_size * countOfPage;

LPVOID virtualpage;
LPVOID page[countOfPage];

int _tmain(int argc, _TCHAR* argv[])
{
	cout << "Lab work 4: allocate virtual memory" << endl;
	cout << "- - - - - - - - - - - - - - - - - - - - - - - - - -" << endl;
	cout << "[step 1]: Create virtual memory." << endl;
	virtualpage = VirtualAlloc(NULL, page_size, MEM_RESERVE, PAGE_READWRITE);

	if (virtualpage == NULL) {
		cout << "Virtual memory doesn't reserved!" << endl;
		cout << GetLastError() << endl;
		system("pause");
		return -1;
	}
	else {
		cout << "Virtual memory was reserved." << endl;
	}
	
	cout << "[step 2]: Create policy." << endl;
	cout << "create READONLY page." << endl;
	page[0] = VirtualAlloc(virtualpage, page_size, MEM_COMMIT, PAGE_READONLY);

	int* arr = (int*)page[0];
	for (int j = 0; j < 2; j++){
		__try {
			cout << "Try to wirte in page..." << endl;
			for (int i = 0; i < 10; i++)
					arr[i] = i;
			cout << "Write succsess." << endl;
			break;
		}
		__except (fixException(GetExceptionCode(), GetExceptionInformation())) {
			cout << "[step 4]: Change policy." << endl;
			PDWORD old;
			DWORD state = PAGE_READONLY;
			old = &state;

			bool status = VirtualProtect(page[0], page_size, PAGE_READWRITE, old);

			if (status) {
				cout << "Succses change policy" << endl;
			}
			else {
				cout << "Fail in change policy" << endl;
			}
		}
	}

	for (int i = 0; i < 10; i++)
		cout << arr[i] << "  ";
	cout << endl;


	cout << "[step 5]: Create two page and move data from one page to another." << endl;
	page[1] = VirtualAlloc(NULL, page_size, MEM_COMMIT | MEM_RESERVE, PAGE_READWRITE);
	page[2] = VirtualAlloc(NULL, page_size, MEM_COMMIT | MEM_RESERVE, PAGE_READWRITE);

	int* memoryBlockA = (int*)page[1];
	int* memoryBlockB = (int*)page[2];

	cout << "- - - - - - - - - - - - - - - - - - - - - - - - - -" << endl;

	for (int i = 0; i < 10; i++)
		for (int j = 0; j < 10; j++)
			memoryBlockA[i * 10 + j] = i * 10 + j;

	for (int i = 0; i < 10; i++) {
		for (int j = 0; j < 10; j++)
			cout << memoryBlockA[i * 10 + j] << (memoryBlockA[i * 10 + j] > 9 ? "  " : "   ");
		cout << endl;
	}

	for (int i = 0; i < 10; i++)
		for (int j = 0; j < 10; j++)
			if (i == j) memoryBlockB[i] = memoryBlockA[i * 10 + j];
	cout << "- - - - - - - - - - - - - - - - - - - - - - - - - -" << endl;

	for (int i = 0; i < 10; i++)
		cout << memoryBlockB[i] << (memoryBlockB[i] > 9 ? "  " : "   ");

	cout << endl << "- - - - - - - - - - - - - - - - - - - - - - - - - -" << endl;

	cout << "Free virtual memory." << endl;
	VirtualFree(virtualpage, page_size, MEM_RELEASE);
	VirtualFree(page[1], page_size, MEM_RELEASE);
	VirtualFree(page[2], page_size, MEM_RELEASE);

	system("pause");
	return 0;
}

int fixException(unsigned int code, struct _EXCEPTION_POINTERS *ep) {
	cout << "[step 3]: Handle problem." << endl;
	cout << "Trying to found a problem." << endl;
	if (code == EXCEPTION_ACCESS_VIOLATION) {
		cout << "Page on READONLY state." << endl;
		return EXCEPTION_EXECUTE_HANDLER;
	}
	else return EXCEPTION_CONTINUE_SEARCH;
}